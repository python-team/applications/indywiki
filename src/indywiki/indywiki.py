#!/usr/bin/env python


import sys
import urllib2, urllib
import threading
from xml.parsers import expat
from xml.dom import minidom
import StringIO
import gzip
import re
import time
from PyQt4 import QtCore, QtGui
from web1 import wikipedia
import qrc_resources
import net
from release import __version__

#at the moment these Wikipedias are the biggest ones. if you want you can add
#any wikipedia (check the list at http://www.wikipedia.org) provided that
#you follow the syntax: "language_code" : "language"

languages = {
    "en" : "English",
    "de" : "German",
    "fr" : "French",
    "it" : "Italian",
    "es" : "Spanish",
    "ja" : "Japanese",
    "pt" : "Portuguese",
    "pl" : "Polish",
    "el" : "Greek",
    "sv" : "Swedish",
    "nl" : "Dutch",
    "tr" : "Turkish",
    "la" : "Latin",
    "fi" : "Finnish",
    "ca" : "Catalan",
    "eu" : "Basque",
    "no" : "Norwegian",
    "ru" : "Russian",
}



class Ui_MainWindow(object):
    def setupUi(self, MainWindow, app, config):
        Ui_MainWindow.ui = self
        Ui_MainWindow.config = config
        Ui_MainWindow.window = MainWindow
        self.app = app
        MainWindow.setObjectName("MainWindow")
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        palette = QtGui.QPalette()
        self.appwidth = QtGui.QApplication.desktop().availableGeometry().width()

        #we define these dictionaries here, that store all sort of data
        self.pushButtonList = {}
        self.image = {}
        self.imagedata = {}
        self.imageurl = {}
        self.response = {}
        self.ttemp = {}
        self.category = {}
        self.searchKeywords = []
        self.number_of_ten = -1

        for i in range(10):
            self.pushButtonList[i] = self.MYB(self.centralwidget, self.appwidth)
            self.pushButtonList[i].setObjectName("pushButton_%d" % i)
            self.pushButtonList[i].setPalette(palette)
            self.pushButtonList[i].setCursor(QtGui.QCursor(
                QtCore.Qt.CursorShape(13)))
        self.lineEdit = self.MYLINEEDIT(self.centralwidget, self.appwidth)
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit.setToolTip(QtGui.QApplication.translate("MainWindow", \
                "Enter a keyword to search for, like Athens, Dublin ...", \
                None, QtGui.QApplication.UnicodeUTF8))


        self.MyTextBrowser1 = self.MyTextBrowser(self.centralwidget)
        self.showMyTextBrowser1Text()
        self.MyTextBrowser1.setObjectName("MyTextBrowser1")

        self.centralTextBrowser = self.MYT(self.centralwidget)
        self.centralTextBrowser.setObjectName("centralTextBrowser")

        self.MyTextBrowser2 = self.MyTextBrowser2(self.centralwidget)
        self.showMyTextBrowser2Text()
        self.MyTextBrowser2.setObjectName("MyTextBrowser2")

        self.centralwidget.first_click = self.first_click
        self.centralTextBrowser.display_images = self.display_images


        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1252, 25))
        self.menubar.setObjectName("menubar")

        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuEdit = QtGui.QMenu(self.menubar)
        self.menuEdit.setObjectName("menuEdit")
        
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        MainWindow.setMenuBar(self.menubar)

        self.qtoolbar = QtGui.QToolBar(MainWindow)
        self.qtoolbar.setObjectName("toolBar")
        MainWindow.addToolBar(self.qtoolbar)	
        self.browserButton = QtGui.QToolButton(self.centralwidget)
        self.browserButton.setIcon(QtGui.QIcon(":/browser.png"))
        self.browserButton.setObjectName("browser")
        self.translationsButton = QtGui.QToolButton(self.centralwidget)
        self.translationsButton.setIcon(QtGui.QIcon(":/trans.png"))
        self.translationsButton.setObjectName("translationsButton")
        self.backButton = QtGui.QToolButton(self.centralwidget)
        self.backButton.setIcon(QtGui.QIcon(":/back.png"))
        self.backButton.setObjectName("backButton")
        self.previousButton = QtGui.QToolButton(self.centralwidget)
        self.previousButton.setIcon(QtGui.QIcon(":/left.png"))
        self.previousButton.setObjectName("pushButton_previous")
        self.nextButton = QtGui.QToolButton(self.centralwidget)
        self.nextButton.setIcon(QtGui.QIcon(":/right.png"))
        self.nextButton.setObjectName("pushButton_next")
        self.goButton = QtGui.QToolButton(self.centralwidget)
        self.goButton.setIcon(QtGui.QIcon(":/zoom.png"))
        self.goButton.setObjectName("pushButton_zoom")
        self.comboBox = QtGui.QComboBox(self.centralwidget)
        self.comboBox.setObjectName("comboBox")

        self.comboBox.setToolTip(QtGui.QApplication.translate("MainWindow", \
                "Choose Wikipedia site", \
                None, QtGui.QApplication.UnicodeUTF8))



        QtCore.QObject.connect(self.browserButton, QtCore.SIGNAL("clicked()"), \
            self.browserButtonClicked, QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.translationsButton, QtCore.SIGNAL(
            "clicked()"), self.showTranslations, QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.backButton, QtCore.SIGNAL(
            "clicked()"), self.backButtonClicked, QtCore.Qt.QueuedConnection)


        self.qtoolbar.addWidget(self.backButton)
        self.qtoolbar.addWidget(self.browserButton)
        self.qtoolbar.addWidget(self.translationsButton)
        self.qtoolbar.addWidget(self.lineEdit)
        self.qtoolbar.addWidget(self.goButton)
        self.qtoolbar.addWidget(self.comboBox)
        self.qtoolbar.addWidget(self.previousButton)
        self.qtoolbar.addWidget(self.nextButton)

        self.browserButton.setToolTip(QtGui.QApplication.translate(
                "MainWindow", "Open page in external browser",
                None, QtGui.QApplication.UnicodeUTF8))
        self.translationsButton.setToolTip(QtGui.QApplication.translate(
                "MainWindow", "View article translations",
                None, QtGui.QApplication.UnicodeUTF8))
        self.backButton.setToolTip(QtGui.QApplication.translate(
                "MainWindow", "Go back one search",
                None, QtGui.QApplication.UnicodeUTF8))

        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.pb = self.MYPB(self.statusbar)
        self.statusbar.addPermanentWidget(self.pb)

        self.pb.setRange(0, 10)
        self.pb.setValue(0)
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionExit.setShortcut('Ctrl+Q')
        self.actionExit.setStatusTip('Exit application')
        QtCore.QObject.connect(self.actionExit, QtCore.SIGNAL(
                'triggered()'), self.app.quit)
        # make sure the config file is saved on exit
        def cleanup():
            self.config.save()
            self.app.quit()
        QtCore.QObject.connect(self.app, QtCore.SIGNAL('aboutToQuit()'), cleanup)
        self.actionProgressBar = QtGui.QAction(MainWindow)
        self.actionProgressBar.setObjectName("actionProgressBar")
        self.actionProgressBar.setCheckable(True)
        self.actionProgressBar.setStatusTip('Toggle visibility of the progress bar')
        QtCore.QObject.connect(self.actionProgressBar, QtCore.SIGNAL(
                'triggered()'), self.toggle_progress_bar)
        self.actionNonNativeColour = QtGui.QAction(MainWindow)
        self.actionNonNativeColour.setObjectName("actionNonNativeColour")
        self.actionNonNativeColour.setStatusTip(
                'Change the background colour of non-native images' +
                '(ie, images in pages pointed to/from the current page)')
        QtCore.QObject.connect(self.actionNonNativeColour, QtCore.SIGNAL(
                'triggered()'), self.setNNimageBgColor)
        self.actionLoadLatestPage = QtGui.QAction(MainWindow)
        self.actionLoadLatestPage.setObjectName("actionLoadLatestPage")
        self.actionLoadLatestPage.setCheckable(True)
        self.actionLoadLatestPage.setStatusTip(
                "When enabled, indywiki will load the latest visited page " +
                "upon the next startup")
        QtCore.QObject.connect(self.actionLoadLatestPage, QtCore.SIGNAL(
                'triggered()'), self.toggleLoadLatestPage)
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionAbout.setStatusTip('About')
        self.actionAbout.setShortcut('F2')
        self.actionHELP = QtGui.QAction(MainWindow)
        self.actionHELP.setObjectName("actionHELP")
        self.actionHELP.setShortcut('F1')
        self.actionHELP.setStatusTip('Help')
        self.actionCredits =  QtGui.QAction(MainWindow)
        self.actionCredits.setObjectName("actionCredits")
        self.actionCredits.setStatusTip('Credits')
        self.menuFile.addAction(self.actionExit)
        self.menuEdit.addAction(self.actionProgressBar)
        self.menuEdit.addAction(self.actionNonNativeColour)
        self.menuEdit.addAction(self.actionLoadLatestPage)
        self.menuHelp.addAction(self.actionHELP)
        self.menuHelp.addAction(self.actionAbout)
        self.menuHelp.addAction(self.actionCredits)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.retranslateUi(MainWindow)

        grid = QtGui.QGridLayout()
        grid.setColumnMinimumWidth(0, 10)
        grid.setColumnMinimumWidth(2, 10)
        grid.setColumnMinimumWidth(4, 10)
        grid.setColumnMinimumWidth(6, 10)
        grid.setColumnMinimumWidth(8, 10)
        grid.setColumnMinimumWidth(10, 10)

        grid.setColumnStretch(0, 2)
        grid.setColumnStretch(2, 2)
        grid.setColumnStretch(4, 2)
        grid.setColumnStretch(6, 2)
        grid.setColumnStretch(8, 2)
        grid.setColumnStretch(10, 2)

        grid.setRowMinimumHeight(2, 10)
        grid.setRowMinimumHeight(4, 10)
        grid.setRowMinimumHeight(6, 10)
        grid.setRowStretch(0, 10)
        grid.setRowStretch(2, 10)
        grid.setRowStretch(6, 10)

        grid.addWidget(self.pushButtonList[0], 1, 1)
        grid.addWidget(self.pushButtonList[1], 1, 3)
        grid.addWidget(self.pushButtonList[2], 1, 5)
        grid.addWidget(self.pushButtonList[3], 1, 7)
        grid.addWidget(self.pushButtonList[4], 1, 9)

        grid.addWidget(self.pushButtonList[5], 3, 1)
        grid.addWidget(self.pushButtonList[6], 3, 3)
        grid.addWidget(self.pushButtonList[7], 3, 5)
        grid.addWidget(self.pushButtonList[8], 3, 7)
        grid.addWidget(self.pushButtonList[9], 3, 9)

        grid.addWidget(self.centralTextBrowser, 7, 3, 7, 5)
        grid.addWidget(self.MyTextBrowser1, 7, 1, -1, 1)
        grid.addWidget(self.MyTextBrowser2, 7, 9, -1, 1)

        self.centralwidget.setLayout(grid)
        MainWindow.setCentralWidget(self.centralwidget)


        #this palette is used to change the background of displayed images
        #that don't belong to the keyword we're looking at...
        self.paletteN = QtGui.QPalette()
        self.brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        self.brush.setStyle(QtCore.Qt.SolidPattern)
        self.paletteN.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button,
                self.brush)
        self.brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        self.brush.setStyle(QtCore.Qt.SolidPattern)
        self.paletteN.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button,
                self.brush)
        self.brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        self.brush.setStyle(QtCore.Qt.SolidPattern)
        self.paletteN.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button,
                self.brush)

        QtCore.QObject.connect(self.lineEdit,
                QtCore.SIGNAL("returnPressed()"), self.first_click,
                QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.centralwidget,
                QtCore.SIGNAL("our_signal_one()"), self.first_clicka,
                QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.MyTextBrowser2,
                QtCore.SIGNAL("our_signal_two()"), self.display_images,
                QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.MyTextBrowser2,
                QtCore.SIGNAL("our_signal_three()"), self.display_images_2,
                QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.centralwidget,
                QtCore.SIGNAL("no_results()"), self.no_results,
                QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.centralwidget,
                QtCore.SIGNAL("no_results_two()"), self.no_results_two,
                QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.centralwidget,
                QtCore.SIGNAL("show_native(int)"), self.show_native,
                QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.centralwidget,
                QtCore.SIGNAL("show_nonnative(int)"), self.show_nonnative,
                QtCore.Qt.QueuedConnection)

        self.handlers = [self.genButtonHandler(i) for i in range(10)]
        for i in range(10):
            QtCore.QObject.connect(self.pushButtonList[i],
                    QtCore.SIGNAL("clicked()"),
                    self.handlers[i], QtCore.Qt.QueuedConnection)

        QtCore.QObject.connect(self.goButton, QtCore.SIGNAL("clicked()"),
                self.goButtonClicked, QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.nextButton, QtCore.SIGNAL("clicked()"),
                self.nextButtonClicked, QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.previousButton, QtCore.SIGNAL("clicked()"),
                self.previousButtonClicked, QtCore.Qt.QueuedConnection)
        QtCore.QObject.connect(self.actionHELP, QtCore.SIGNAL('triggered()'),
                self.help)
        QtCore.QObject.connect(self.actionAbout, QtCore.SIGNAL('triggered()'),
                self.about)
        QtCore.QObject.connect(self.actionCredits,
                QtCore.SIGNAL('triggered()'), self.credits)
        QtCore.QObject.connect(self.centralwidget,
                QtCore.SIGNAL("progressChanged(int)"),
                self.update_progress, QtCore.Qt.QueuedConnection )

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow",
                "Indywiki: exploring wikipedia visually", None,
                QtGui.QApplication.UnicodeUTF8))
        geometry, homePage = self.config.load()
        self.actionLoadLatestPage.setChecked(self.config.load_latest_page)
        self.actionProgressBar.setChecked(self.config.show_progress_bar)
        if self.config.show_progress_bar:
            self.pb.show()
        else:
            self.pb.hide()
        if not geometry:
            if self.appwidth == 1400:
                geometry = (60, 40, 1241, 890)
            elif self.appwidth == 1280:
                geometry = (30, 20, 1091, 850)
            elif self.appwidth == 1152:
                geometry = (20, 10, 1060, 750)
            elif self.appwidth == 1024:
                geometry = (30, 20, 941, 630)
            else:
                geometry = (10, 10, 731, 540)
        apply(MainWindow.setGeometry, geometry)

        #we want the first item in the combobox to be our language of choice
        self.showCombobox()


        MainWindow.setWindowIcon(QtGui.QIcon(':/logo.jpg'))
        self.goButton.setText(QtGui.QApplication.translate(
                "MainWindow", "GO", None, QtGui.QApplication.UnicodeUTF8))
        self.goButton.setToolTip(QtGui.QApplication.translate(
                "MainWindow", "Start the search!", None,
                QtGui.QApplication.UnicodeUTF8))
        self.nextButton.setText(QtGui.QApplication.translate("MainWindow",
                "Next", None, QtGui.QApplication.UnicodeUTF8))
        self.nextButton.setToolTip(QtGui.QApplication.translate(
                "MainWindow", "Press to display the next 10 images",
                None, QtGui.QApplication.UnicodeUTF8))
        self.previousButton.setText(QtGui.QApplication.translate(
                "MainWindow", "Previous", None, QtGui.QApplication.UnicodeUTF8))
        self.previousButton.setToolTip(QtGui.QApplication.translate(
                "MainWindow", "Press to display the previous 10 images",
                None, QtGui.QApplication.UnicodeUTF8))
        self.centralTextBrowser.setHtml(QtGui.QApplication.translate("MainWindow", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
        "p, li { white-space: pre-wrap; }\n"
        "</style></head><body style=\" font-family:\'Helvetica\'; font-size:11pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))


        self.menuFile.setTitle(QtGui.QApplication.translate("MainWindow",
            "&File", None, QtGui.QApplication.UnicodeUTF8))
        self.menuEdit.setTitle(QtGui.QApplication.translate("MainWindow",
            "&Edit", None, QtGui.QApplication.UnicodeUTF8))        
        self.menuHelp.setTitle(QtGui.QApplication.translate("MainWindow",
            "&Help", None, QtGui.QApplication.UnicodeUTF8))
        self.actionExit.setText(QtGui.QApplication.translate("MainWindow",
            "&Exit", None, QtGui.QApplication.UnicodeUTF8))
        self.actionProgressBar.setText(QtGui.QApplication.translate(
            "MainWindow", "&Toggle Progress bar", None,
            QtGui.QApplication.UnicodeUTF8))
        self.actionNonNativeColour.setText(QtGui.QApplication.translate(
            "MainWindow", "&Change background colour of non-native images", None,
            QtGui.QApplication.UnicodeUTF8))
        self.actionLoadLatestPage.setText(QtGui.QApplication.translate(
            "MainWindow", "&Load latest page on startup", None,
            QtGui.QApplication.UnicodeUTF8))
        self.actionHELP.setText(QtGui.QApplication.translate("MainWindow",
            "&Help", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAbout.setText(QtGui.QApplication.translate("MainWindow",
            "&About", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCredits.setText(QtGui.QApplication.translate("MainWindow",
            "&Credits", None, QtGui.QApplication.UnicodeUTF8))

        #800x600 is the most screen resolution
        iconSizes = { 1400 : (215, 165),
                      640 : (130, 100),
                      1024 : (170, 130),
                      1152 : (190, 150),
                      1280 : (211,152) }
        try:
            width, height = iconSizes[self.appwidth]
        except KeyError:
            width, height = (130, 100)
        for i in range(10):
            self.pushButtonList[i].setIconSize(QtCore.QSize(width, height))

        #init with some free media images
        for i in range(10):
            self.pushButtonList[i].setIcon(QtGui.QIcon(":/wikipedia.jpg"))

        self.centralTextBrowser.insertHtml("<html><body style=\" font-family:\'Helvetica\'; font-size:12pt; \">\n"
        "<p style=\"   \">  Enter a keyword and search through the wonderful world of Wikipedia!</body></html>")

        #set the last article as the home page
        if homePage and self.config.load_latest_page:
            self.setPage(homePage)
    
    def setPage(self,name):
        """sets a home page"""
        self.lineEdit.insert(name.decode("utf8"))
        self.centralwidget.first_click()


    def genButtonHandler(self, i):
        """what we are doing in case one of the ten buttons, or the next
        button is pressed"""
        def buttonClicked():
            def extractImageName(url):
                """returns a meaningful part from an image's url; eg these
                    http://upload.wikimedia.org/wikipedia/en/2/20/Ikaria_greece.png
                    http://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Armenistis.JPG/250px-Armenistis.JPG
                    would be parsed to Ikaria_greece.png and Armenistis.JPG
                    http://en.wikipedia.org/wiki/Help:Images_and_other_uploaded_files
                    describes supported image files
                """
                imageName = url.split('/')[-2]
                if imageName.split('.')[-1].lower() not in ['svg','png', \
                        'jpg','gif','jpeg']: 
                    imageName = url.split('/')[-1]
                return imageName
                
            if self.number_of_ten == -1:
                return
            if self.number_of_ten == 0:
                if type(self.dicta["%s" % i]) is list:
                    self.display_native_size(extractImageName(self.dicta["%s" % i][1]),
                            self.dicta["%s" % i][0])
                elif type(self.dicta["%s" % i]) is tuple:
                    self.lineEdit.clear()
                    self.lineEdit.insert(QtCore.QString( \
                        self.dicta["%s" % i][0]))
                    self.centralwidget.first_click()
            else:
                if type(self.dicta['%s' % str(self.number_of_ten)+str(i)]) \
                        is list:
                    self.display_native_size(extractImageName(self.dicta['%s' % str( \
                        self.number_of_ten)+str(i)][1]), \
                        self.dicta["%s" % str(self.number_of_ten)+str(i)][0])
                elif type(self.dicta['%s' % str(self.number_of_ten)+str(i)]) is tuple:
                    self.lineEdit.clear()
                    self.lineEdit.insert(QtCore.QString(self.dicta['%s' % \
                        str(self.number_of_ten)+str(i)][0]))
                    self.centralwidget.first_click()
        return buttonClicked


    def goButtonClicked(self):
        self.first_click()

    def backButtonClicked(self):
        if not self.searchKeywords or len(self.searchKeywords) == 1:
            return
        self.lineEdit.clear()
        self.searchKeywords.remove(self.searchKeywords[0])
        self.lineEdit.insert(QtCore.QString(self.searchKeywords[0][1]))
        self.config.language = self.searchKeywords[0][0]
        self.searchKeywords.remove(self.searchKeywords[0])
        self.showCombobox()
        self.first_click()


    def browserButtonClicked(self):
        """Open the current page to an independent browser thread."""
        if not self.searchKeywords:
            return
        class ExternalBrowser(threading.Thread):
            def __init__(self, url):
                self.url = url
                threading.Thread.__init__(self)
            def run(self):
                import webbrowser
                try:
                    webbrowser.open_new_tab(self.url)
                except AttributeError: # if earlier than python 2.5
                    webbrowser.open_new(self.url)
        ExternalBrowser('http://'+self.config.language+'.wikipedia.org/wiki/'+self.searchKeywords[0][1]).start()

    def nextButtonClicked(self):
        if self.number_of_ten == -1:
            return
        try:
            self.pb.reset()
            self.centralwidget.emit(QtCore.SIGNAL("progressChanged(int)"), 2)
            self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.BusyCursor))
            net.get_next_ten(Ui_MainWindow.ui, self.number_of_ten)
        except NameError:
            self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))

    def previousButtonClicked(self):
        """displays the first ten images"""
        if self.number_of_ten in [0, -1]:
            return
        self.pb.reset()
        self.centralwidget.emit(QtCore.SIGNAL("progressChanged(int)"), 2)
        self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.BusyCursor))
        self.number_of_ten -= 1
        for i in range((self.number_of_ten)*10, (self.number_of_ten+1)*10):
            Ui_MainWindow.ui.centralwidget.emit(QtCore.SIGNAL("show_native(int)"), i)

    def first_click(self):
        self.config.language = [item[0] for item in wikipedia.languages.items() \
                if item[1] == unicode(self.comboBox.currentText())][0]
        self.statusbar.showMessage("")
        self.pb.reset()
        self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.BusyCursor))
        self.initialKeyword = unicode(self.lineEdit.text()) \
                .replace('%20', '_').replace(' ', '_') 
        self.searchKeywords.insert(0, [self.config.language, unicode(self.lineEdit.text()) \
                .replace('%20', '_').replace(' ', '_')])
        self.key_word = self.searchKeywords[0][1]
        Ui_MainWindow.ui.first_click_zero().start()

    

    def first_clicka(self):
        """start the thread(s) to get links on each of the article's
        categories"""        
        m = 0
        if not self.aquery.categories:
            net.getCategories(Ui_MainWindow.ui)
        for i in self.aquery.categories:
            self.get_categories(i, m).start()
            m += 1
        self.MyTextBrowser1.clear()
        self.showMyTextBrowser1Text()        
        self.centralwidget.emit(QtCore.SIGNAL("progressChanged(int)"), 2)
        #store the values for next time we use indywiki 
        self.config.geometry = [Ui_MainWindow.window.geometry().x(),
                                Ui_MainWindow.window.geometry().y(),
                                Ui_MainWindow.window.geometry().width(),
                                Ui_MainWindow.window.geometry().height()]
        self.config.homePage = Ui_MainWindow.ui.searchKeywords[0][1]
        self.config.save()
        self.temp_var1 = 0
        paragraphsStr = ''
        for paragraph in self.aquery.paragraphs:
            paragraphsStr += """ <ul> <a href='%s'>%s</a> </ul><br> """ \
                    %(self.temp_var1, paragraph.title)
            self.temp_var1 += 1
        paragraphsStr += """ <ul> <a href='%s'>References</a> </ul><br> """ \
                % self.temp_var1
        self.MyTextBrowser1.insertHtml(paragraphsStr)        
        self.centralTextBrowser.clear()
        try:
            self.temp = unicode(str(self.aquery.paragraphs[0]).decode("utf8"))
        except IndexError:
            return
        self.temp = re.sub("\n", "<br>", self.temp)
        self.centralTextBrowser.insertHtml(self.temp)
        self.MyTextBrowser2.clear()
        self.showMyTextBrowser2Text()
        self.linksa = self.aquery.links
        #Each article contains links to dates, and some articles have lots
        #of them. We want them to be displayed at the end of the list
        #Obviously this can be a one liner!
        self.linkTemp1 = [link for link in self.linksa if \
                re.search(r"[0-9]+",link)]
        self.linkTemp2 = [link for link in self.linksa if not \
                re.search(r"[0-9]+",link)]
        self.linksa = self.linkTemp2
        self.linksa.extend(self.linkTemp1)
        linksStr = ''
        for link in self.linksa:
            linksStr +=  """ <a href='%s'>%s</a><br> """ % (link,link)
        self.MyTextBrowser2.insertHtml(linksStr)
        self.centralwidget.emit(QtCore.SIGNAL("progressChanged(int)"), 1)
        self.MyTextBrowser2.emit(QtCore.SIGNAL("our_signal_two()"))



#Here's the logic in emitting signals:centralwidget emits show_native
#signals (with the value) for "native" images and show_nonnative
#(with the value) for "not native" images...

    def display_images(self):
        """empties the list with displayed_so_far photos, since we start a new
         search"""
        self.displayed_so_far = []
        self.number_of_ten = 0
        self.dicta = net.getImages(self.searchKeywords[0][1].encode("utf8"),
                Ui_MainWindow.ui)
        #not sure if this has to change and use nodebox's capabilities
    #instead of get_images()
        if len(self.dicta)>=10:
            for i in range(10):
                self.Download_native(i).start()
        else:
            dict_len = len(self.dicta)
            for i in range(dict_len):
                self.Download_native(i).start()
        self.MyTextBrowser2.emit(QtCore.SIGNAL("our_signal_three()"))
        self.statusbar.showMessage("Indywiki search on http://"+self.config.language+ \
        ".wikipedia.org. Article "+self.searchKeywords[0][1]+" has "+ \
        str(len(self.dicta))+" images.")


    def display_images_2(self):
        url = "http://"+self.config.language+".wikipedia.org/w/api.php?action=query&generator=backlinks&format=xml&gbllimit=500&gbltitle="+self.searchKeywords[0][1].encode("utf8")
        dom_elements = net.downloadAndParse(url, self.config.agent_id)
        self.listaa = [e.attributes["title"].value for e in dom_elements]
        self.sorted_list = self.filter_links()
        if len(self.dicta)<10:
            net.get_first_ten(Ui_MainWindow.ui)

    def updateButtonColor(self, button):
        if button.isNative:
            button.setPalette(QtGui.QPalette(QtGui.QColor(230,230,250)))
        else:
            button.setPalette(QtGui.QPalette(self.getNNimageBgColor()))

    def show_native(self, i):
        """by native we mean they are founded on the link we're looking at"""
        self.centralwidget.emit(QtCore.SIGNAL("progressChanged(int)"), 1)
        self.temp0 = self.dicta['%s' % i]
        index = int(str(i)[-1])

        self.image[i] = QtGui.QPixmap()
        self.image[i].loadFromData(self.imagedata[i])
        self.pushButtonList[index].setIcon(QtGui.QIcon(self.image[i]))
        self.pushButtonList[index].isNative = True
        try:
            #a native image ['Richard Stallman, founder of the GNU project',
            #ImageURL, '180', '136']
            #a non_native image (Article, ['Richard Stallman, founder of the 
            #GNU project', ImageURL, '180', '136'])
            if isinstance(self.temp0, list):
                tooltip = str(self.temp0[0].replace('\'', ''))
            elif isinstance(self.temp0, tuple):
                self.pushButtonList[index].isNative = False
                tooltip = "<b>"+str(self.temp0[0].replace('\'','')+"</b>"+': '+self.temp0[1][0].replace('\'',''))
            self.pushButtonList[index].setToolTip( \
                    QtGui.QApplication.translate('MainWindow', tooltip, \
                    None, QtGui.QApplication.UnicodeUTF8))                  
        except UnicodeDecodeError:
            pass
        self.updateButtonColor(self.pushButtonList[index])
        #this was added in case we are pressing the Previous/Next buttons. since
        #images have already been fetched, no need to call class Download_native
        #again. show_native when called doesn't know if we are pressing 
        #these buttons or not, so we ask if the entry on the dict is
        #list (native) or tuple (previous/next button)
        self.displayed_so_far.append(self.temp0[1][1])
        if index == 9:
            self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))

    def show_nonnative(self, i):
        """while by nonnative we mean images found on links of our link"""
        self.centralwidget.emit(QtCore.SIGNAL("progressChanged(int)"), 1)
        self.temp0 = self.dicta['%s' % i]
        index = int(str(i)[-1])

        self.image[i] =  QtGui.QPixmap()
        self.image[i].loadFromData(self.imagedata[i])
        self.pushButtonList[index].setIcon(QtGui.QIcon(self.image[i]))
        self.pushButtonList[index].isNative = False
        self.updateButtonColor(self.pushButtonList[index])
        try:
            tooltip = "<b>"+str(self.temp0[0].replace('/wiki/','').replace('\'','')+"</b>"+': '+self.temp0[1][0].replace('\'',''))
            self.pushButtonList[index].setToolTip(
                QtGui.QApplication.translate('MainWindow', tooltip,
                None, QtGui.QApplication.UnicodeUTF8))
        except UnicodeDecodeError:
            pass
        self.displayed_so_far.append(self.temp0[1][1])
        #self.pushButtonList[index].setPalette(self.paletteN)
        #not sure if we're going to use it or not...

        if index == 9:
            self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))




#this function tries to sort the links, following this algorithm:
#A (links to)------>wikipedia article---------->B (links from)
#                   C category1
#                   D category 2
#                  .
#                  .
#                  X category xx
#it gives these scores:
#A: 2, B:2, {C, D...X}:1 , A+B:4, A+B+C:5, A+D:3 etc , trying to give
#the higher scores to the most related items
#
#alist: A+B
#blist: B
#clist: A                self.printme= get_images(self.url.replace('%20','_').replace(' ','_').encode("utf8"))
# we get A thanks to the Wikipedia API, and B with self.aquery.links
#suggestions: A+B = 5 since it shows a bydirectional relationship


    def filter_links(self):
        self.listaa = [i for i in self.listaa if i[:4] not in ['User', 'Talk'] \
        and i[:9] != 'Wikipedia']
        alist = [x for x in self.linksa if x in self.listaa]
        blist = [y for y in self.linksa if y not in self.listaa]
        clist = [z for z in self.listaa if z not in self.linksa \
        and z[:5] != 'User:']

        #make dictionaries from lists
        alist = dict([(n, 4) for n in alist])
        blist = dict([(n, 2) for n in blist])
        clist = dict([(n, 2) for n in clist])

        self.categories_list = []

        for i in range(len(self.aquery.categories)):
            if not self.category[i]: time.sleep(1)
            self.categories_list.append(self.category[i])
        

        #raise the score of an entry, if it appears on one or more categories

        for lst in (alist, blist, clist):
            for key in lst.iterkeys():
                for category in self.categories_list:
                    if key in category:
                        lst[key] += 1
        alist.update(blist)
        alist.update(clist)

        #convert from dictionary into sorted list
        self.sorted_links = [(v, k) for k, v in alist.items()]
        self.sorted_links.sort(reverse=True)
        self.sorted_links = [(k, v) for v, k in self.sorted_links]

        self.sorted_links = [link[0] for link in self.sorted_links]

        for i in range(len(self.aquery.categories)):
            try:
                self.sorted_links.extend(self.category[i])
            except KeyError:
                print 'ignoring missing category key %d' % i


        return self.sorted_links




    def no_results(self):
        """ display some text on the GUI and are called by class first_click_zero, that can't make direct changes to the GUI"""
        self.pb.reset()
        self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.statusbar.showMessage("No matches while searching http://"+self.config.language+".wikipedia.org. Perhaps a misspell?")
        self.centralTextBrowser.clear()
        self.MyTextBrowser1.clear()
        self.showMyTextBrowser1Text()
        self.MyTextBrowser2.clear()
        self.showMyTextBrowser2Text()
        self.possibleLinks = []
        urllib.URLopener.version = Ui_MainWindow.config.agent_id
        usock = urllib.urlopen("http://"+self.config.language+".wikipedia.org/w/api.php?action=query&list=search&srsearch="+str(Ui_MainWindow.ui.initialKeyword)+"&srwhat=text&srnamespace=0&format=xml&srlimit=50")   
        xmldoc = minidom.parse(usock)
        usock.close()
        dom = minidom.parseString(xmldoc.toxml().encode("utf8"))
        self.tempLinks = dom.getElementsByTagName('p')
        for i in range(len(self.tempLinks)):
            self.possibleLinks.append(self.tempLinks[i].attributes['title'].value)
        if self.tempLinks:
            searchItemsStr = '<h2>The term <i> %s</i> may refer to:</h2><br><br>' % Ui_MainWindow.ui.initialKeyword
            for possibleLink in self.possibleLinks: 
                searchItemsStr += " <a href='%s'>  %s</a> <br>" % (possibleLink, possibleLink)
            self.centralTextBrowser.insertHtml(searchItemsStr)



            

    def no_results_two(self):
        """ display some text on the GUI and are called by class first_click_zero, that can't make direct changes to the GUI"""
        self.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.statusbar.showMessage("Can't find anything at http://"+self.config.language+".wikipedia.org/wiki/"+self.searchKeywords[0][1]+", or there are network connection problems.")





    class first_click_zero(threading.Thread):
        """ tries to get the text of our keyword search"""
        def __init__(self):
            threading.Thread.__init__(self)

        def run(self):
            try:
                Ui_MainWindow.ui.aquery = net.wikipedia.search("%s" % str(Ui_MainWindow.ui.searchKeywords[0][1]), \
                language=Ui_MainWindow.config.language, light=False)
            except (IndexError, expat.ExpatError):
                Ui_MainWindow.ui.centralwidget.emit(QtCore.SIGNAL("no_results_two()"))
                return
            if len(Ui_MainWindow.ui.aquery.paragraphs) == 0:
                #redirect
                self.request = urllib2.Request("http://"+Ui_MainWindow.config.language+ \
                ".wikipedia.org/wiki/Special:Search/"+Ui_MainWindow.ui.searchKeywords[0][1], \
                        None, Ui_MainWindow.config.http_headers)
                self.opener = urllib2.build_opener()
                self.f = self.opener.open(self.request)
                Ui_MainWindow.ui.searchKeywords[0][1] = self.f.url.split('/wiki/')[-1]
                try:
                    Ui_MainWindow.ui.aquery = net.wikipedia.search("%s" % str(Ui_MainWindow.ui.searchKeywords[0][1]), \
                    language=Ui_MainWindow.config.language, light=False)
                except IndexError:
                    Ui_MainWindow.ui.centralwidget.emit(QtCore.SIGNAL("no_results()"))
                    return
                if not Ui_MainWindow.ui.aquery.paragraphs: 
                    Ui_MainWindow.ui.centralwidget.emit(QtCore.SIGNAL("no_results()"))
                    return
                self.opener.close()
                self.f.close()            
            Ui_MainWindow.ui.centralwidget.emit(QtCore.SIGNAL("our_signal_one()"))




    class Download_native(threading.Thread):
        def __init__(self, n):
            self.n = n
            threading.Thread.__init__(self)
        def run(self):
            if type(Ui_MainWindow.ui.dicta['%s' % self.n]) is list:
                self.temp0 = Ui_MainWindow.ui.dicta['%s' % self.n]
            elif type(Ui_MainWindow.ui.dicta['%s' % self.n]) is tuple:
                self.temp0 = Ui_MainWindow.ui.dicta['%s' % self.n][1]
            #this was added in case we are pressing the Previous button. in most
            #cases this class is asked to fetch and display an image that is
            #'native'. In the case of Previous button, get_next_ten uses this
            #class, since it finds that the entries exist on the dict -aka
            #there are the images to display-, though it doesn't know that
            #they are 'nonnative'
            Ui_MainWindow.ui.imageurl[self.n] = urllib2.Request(self.temp0[1],
                    None, Ui_MainWindow.config.http_headers)
            try:
                Ui_MainWindow.ui.response[self.n] = urllib2.urlopen( \
                        Ui_MainWindow.ui.imageurl[self.n])
            except urllib2.URLError:
                Ui_MainWindow.ui.response[self.n] = urllib2.urlopen(Ui_MainWindow.ui.imageurl[self.n])
            Ui_MainWindow.ui.imagedata[self.n] = Ui_MainWindow.ui.response[self.n].read()
            Ui_MainWindow.ui.response[self.n].close()
            Ui_MainWindow.ui.ttemp[self.n] = Ui_MainWindow.ui.Downloadd_native(self.n)
            Ui_MainWindow.ui.ttemp[self.n].start()
            Ui_MainWindow.ui.ttemp[self.n].wait()

    class Downloadd_native(QtCore.QThread):
        def __init__(self, m ):
            QtCore.QThread.__init__(self)
            self.n = m
        def run(self):
            Ui_MainWindow.ui.centralwidget.emit(QtCore.SIGNAL("show_native(int)"), self.n)

    class get_categories(threading.Thread):
        def __init__(self, n, m):
            self.n = n
            self.m = m
            Ui_MainWindow.ui.category[self.m] = []
            threading.Thread.__init__(self)

        def run(self):
            url = "http://"+Ui_MainWindow.config.language+".wikipedia.org/w/api.php?format=xml&action=query&cmlimit=500&list=categorymembers&cmtitle=Category:"+self.n.encode("utf8").replace(' ', '_')
            # category members
            dom_elements = net.downloadAndParse(url,
                    Ui_MainWindow.config.agent_id, "cm")
            try:
                Ui_MainWindow.ui.category[self.m] = [e.attributes['title'].value
                    for e in dom_elements]
            except TypeError: pass

    def help(self):
        dlg = QtGui.QDialog()
        browser = QtGui.QTextBrowser()
        browser.setOpenExternalLinks(True)
        dlg.setWindowIcon(QtGui.QIcon(':/logo.jpg'))
        label = QtGui.QLabel("<font size=8 color=#A0A000>&nbsp;"
                           "<b>Indywiki %s </b></font>" % __version__)
        dlg.setWindowTitle("indywiki - Help")
        browser.setHtml("""\

Indywiki is pretty straightforward to use.<br><br> When the program launches,
write your search term on the box and press enter, or the magnifier icon.
 You will notice that the mouse pointer changes, an indication that something
 is happening.   After a while (depending on your internet connection) text
and images will start appearing on the screen.
On the left text box titles are displayed. If you click on one of them, it's
text will appear on the main text box. Text that appears on the main box is
the summary of our search term, which is usually the first paragraphs
-according to the wikipedia guidelines on how to write an article! <br><br>
The text box located on the right of our program, contains wikipedia links
founded on the article we are searching for. If you press one of them, a new
search will start, with this keyword.

After the first ten images are displayed, place the mouse pointer on one of
them, and you will see the text accompanying the image on the wikipedia
article, if there is one. If the page has less than ten images (or if it
doesn't have any images at all), indywiki searches links of that page and
displays their images, provided they have not been displayed before. If you
notice the column character on one of the titles, the name before the column
refers to the page where we got that image, and the string after the column
is that image's description.

After you have searched for a link and got back the result images, you can
press the right arrow, that is on the upper side of the application.
The application will try to fetch and display the next 10 images,
if they exist, otherwise it will bring the images of the links found on the
page we were looking for.Similarly you can press the left arrow and the
application will display the previous ten images. <br><br>
On the other side, <b> if you click on an image </b> that belongs to an article
that is different than the term we are looking for,  that image's article
becomes our query, thus you will get back the results of that page.
<br>
However, if you click on one image that has been fetched from the article we
were looking for, the image will be displayed on a separate window, at it's
normal size, not as a thumbnail!
<br>
If an article has been translated to other Wikipedia sites, you can easily
browse these translations, by clicking the translations icon, which is the
 third in the toolbar.

<br>Visit  project's page at  <a href="http://indywiki.sourceforge.net">
http://indywiki.sourceforge.net</a>.
""")
        layout = QtGui.QVBoxLayout(dlg)
        layout.addWidget(browser)
        layout.addWidget(label)
        dlg.setMinimumSize(
            min(dlg.width(),
                int(QtGui.QApplication.desktop().availableGeometry() \
                    .width() / 3)),
                int(QtGui.QApplication.desktop().availableGeometry() \
                    .height() /1.3))
        dlg.exec_()



    def about(self):
        adlg = QtGui.QDialog()
        browser = QtGui.QTextBrowser()
        browser.setOpenExternalLinks(True)
        adlg.setWindowIcon(QtGui.QIcon(':/logo.jpg'))
        label = QtGui.QLabel("<font size=6 color=#A0A000>&nbsp;"
                           "<b>Indywiki version %s </b></font>" % __version__)
        adlg.setWindowTitle("indywiki - About")
        browser.setHtml("""
Some times browsing over the plethora of information available through
wikipedia pages becomes a boring activity. <p><i> Indywiki project aims
to explore different ways of visually browsing wikipedia pages. </p></i>
This might be very helpful, especially in cases we are only interested to
get an idea on the term we are searching for. <i> In other words, browsing
through images, while the text is still available to read!</i><br>
<br>Indywiki discovers and displays images that are the most related links
to our search keyword. Images are displayed in tenths. So if we are searching
for a city as an example, after we get all images displayed on the wikipedia
page about this city, we'll start getting links about related items (that
might be other cities in this country, personalities and culture of this city
and practically everything related, the more we keep on getting images.).
To do so  indywiki uses an algorithm that tries to calculate scors for each
article.
<br><br>
Indywiki achieves this by taking under consideration
<p>1)articles that link to our article
<br>2)articles that are linked from our article
<br>3)categories to which our article  and also all articles on 1)
and 2) belong to.
<br><br>All links get a score, that increases if a link is at 1) and 2),
or if it belongs to one or more categories as our article. After indywiki
has discovered the related articles, it parses them and fetches their first
image, in case they have one. Usually the first image is the most
representative of an article.  We can continue the search for as long as we like!

<br><br><br>
See Help for more (or press F1)



 """)
        layout = QtGui.QVBoxLayout(adlg)
        layout.addWidget(browser)
        layout.addWidget(label)
        adlg.setMinimumSize(
            min(adlg.width(),
                int(QtGui.QApplication.desktop().availableGeometry() \
                    .width() / 3)),
                int(QtGui.QApplication.desktop().availableGeometry() \
                    .height() / 1.3))
        adlg.exec_()


    def credits(self):
        adlg = QtGui.QDialog()
        browser = QtGui.QTextBrowser()
        browser.setOpenExternalLinks(True)
        label = QtGui.QLabel("<font size=6 color=#A0A000>&nbsp;"
                           "<b>Indywiki version %s </b></font>" % __version__)
        adlg.setWindowTitle("indywiki - Credits")
        adlg.setWindowIcon(QtGui.QIcon(':/logo.jpg'))
        browser.setHtml("""\
Authors: Markos Gogoulos - mgogoulos@gmail.com,<br>
Serafeim Zanikolas - serzan@hellug.gr <br>

Initial idea inspired by prof. Diomidis Spinellis<br>
<br>Thanks goes to: <br>Victoras Gogas that helped with the gui design
and overal aesthetics
<br>Stefan Rodiger, Vassilios Karakoidas, Alex Tsavdaroglou,
Dimitris Kalamaras, Nikos Roussos
for ideas/beta testing
<br><br><br>
Visit project's page at  <a href="http://indywiki.sf.net">
http://indywiki.sf.net</a> and download latest version.
<br><br>
Please help improve indywiki, by sending suggestions, ideas, code,
 bugs, graphics at mgogoulos@gmail.com """)
        layout = QtGui.QVBoxLayout(adlg)
        layout.addWidget(browser)
        layout.addWidget(label)
        adlg.setMinimumSize(
            min(adlg.width(),
                int(QtGui.QApplication.desktop().availableGeometry() \
                    .width() / 3)),
                int(QtGui.QApplication.desktop().availableGeometry() \
                    .height() / 3))
        adlg.exec_()




    def display_native_size(self, image_url, image_title):
        """finds and displays the image on it's native size (no thumbnail)"""
        adlg = QtGui.QDialog()
        adlg.setWindowIcon(QtGui.QIcon(':/icon.jpg'))
        self.native = net.getImages('Image:'+image_url, Ui_MainWindow.ui)
        imageurl = urllib2.Request(self.native['0'][1], None,
                                    Ui_MainWindow.config.http_headers)
        response = urllib2.urlopen(imageurl)
        imagedata = response.read()
        response.close()
        image = QtGui.QPixmap()
        image.loadFromData(imagedata)
        self.outputLabel = QtGui.QLabel()
        self.outputLabel.setPixmap(image)
        #adlg.setWindowTitle(QtCore.QString(self.native['0'][0].replace('Image:', '')))
        adlg.setWindowTitle(QtCore.QString(str(image_title).decode("utf8")))
        layout = QtGui.QVBoxLayout(adlg)
        layout.addWidget(self.outputLabel)
        self.this_url = QtCore.QString("http://"+Ui_MainWindow.config.language+".wikipedia.org/wiki/Image:"+image_url)
        label = QtGui.QLabel("<a href='%s'>Image link" % self.this_url)
        label.setOpenExternalLinks(True)
        layout.addWidget(label)
        adlg.setMinimumSize(int(self.native['0'][2]), int(self.native['0'][3]))
        adlg.setMaximumSize(int(self.native['0'][2]), int(self.native['0'][3]))
        adlg.exec_()

#subclasses of several Qt widgets follow, so we give them by default
#our own settings

    class MyTextBrowser(QtGui.QTextBrowser):
        """subclass of QTextBrowser class, so that text titles of a wikipedia
    page link to their paragraph text, displayed on textedit2"""
        def __init__(self, parent):
            QtGui.QTextBrowser.__init__(self, parent)
            self.setSizePolicy(QtGui.QSizePolicy( QtGui.QSizePolicy.Maximum,
         QtGui.QSizePolicy.Maximum))

        def setSource(self, number_of_title):
            Ui_MainWindow.ui.centralTextBrowser.clear()
            try:
                paragraph = '<h2>%s</h2><br><br>' % \
                        Ui_MainWindow.ui.aquery.paragraphs[int(number_of_title.toString())].title
                
                paragraphText = unicode(str(Ui_MainWindow.ui.aquery \
                        .paragraphs[int(number_of_title.toString())]) \
                            .decode("utf8"))  
                paragraphText = re.sub("\n", "<br>", paragraphText)
                paragraph += paragraphText
                Ui_MainWindow.ui.centralTextBrowser.insertHtml(paragraph)
            except IndexError:
                referenseStr = '<h2>References</h2><br><br>'
                for referenceNumber, entry in \
                        enumerate(Ui_MainWindow.ui.aquery.references):
                    referenseStr += '['+str(referenceNumber)+'] '+unicode(str(entry).decode("utf8"))+'<br>'
                Ui_MainWindow.ui.centralTextBrowser.insertHtml(referenseStr)

        def sizeHint(self):
            return QtCore.QSize(201, 341)



    class MyTextBrowser2(QtGui.QTextBrowser):
        """subclass of QTextBrowser class, so that links found on a wikipedia
        page become our search keywords"""
        def __init__(self, parent):
            QtGui.QTextBrowser.__init__(self, parent)
            self.setSizePolicy(QtGui.QSizePolicy( QtGui.QSizePolicy.Preferred,
            QtGui.QSizePolicy.Maximum))

        def setSource(self, title):
            Ui_MainWindow.ui.lineEdit.clear()
            Ui_MainWindow.ui.lineEdit.insert(QtCore.QString(title.toString()))
            Ui_MainWindow.ui.centralwidget.first_click()

        def sizeHint(self):
            return QtCore.QSize(192, 341)


    class MYT(QtGui.QTextBrowser):
        def __init__(self, parent):
            QtGui.QTextBrowser.__init__(self, parent)
            self.setSizePolicy(QtGui.QSizePolicy( QtGui.QSizePolicy.Preferred,
            QtGui.QSizePolicy.Maximum))

        def setSource(self, title):
            Ui_MainWindow.ui.lineEdit.clear()
            Ui_MainWindow.ui.lineEdit.insert(QtCore.QString(title.toString()))
            Ui_MainWindow.ui.centralwidget.first_click()
            
        def sizeHint(self):
            return QtCore.QSize(691, 341)



    class MYB(QtGui.QPushButton):
        def __init__(self, parent, appwidth):
            QtGui.QPushButton.__init__(self, parent)
            self.setSizePolicy(QtGui.QSizePolicy( QtGui.QSizePolicy.Maximum,
                QtGui.QSizePolicy.Maximum))
            self.appwidth = appwidth
            self.isNative = True

        def sizeHint(self):
            #800x600 is the most screen resolution
            if int(self.appwidth) == 1400:
                return QtCore.QSize(225, 175)
            elif int(self.appwidth) == 1024:
                return QtCore.QSize(180, 140)
            elif int(self.appwidth) == 1152:
                return QtCore.QSize(200, 160)
            elif int(self.appwidth) == 1280:
                return QtCore.QSize(221,161)
            else:
                return QtCore.QSize(140, 110)   

    class MYLINEEDIT(QtGui.QLineEdit):
        def __init__(self, parent, appwidth):
            QtGui.QLineEdit.__init__(self, parent)
            self.setSizePolicy(QtGui.QSizePolicy( QtGui.QSizePolicy.Fixed,
            QtGui.QSizePolicy.Fixed))
            self.appwidth = appwidth

        def sizeHint(self):
            return QtCore.QSize(self.appwidth/5, 24)

    class MYPB(QtGui.QProgressBar):
        def __init__(self, parent):
            QtGui.QProgressBar.__init__(self, parent)
            self.setSizePolicy(QtGui.QSizePolicy( QtGui.QSizePolicy.Fixed,
            QtGui.QSizePolicy.Fixed))
        def sizeHint(self):
            return QtCore.QSize(200, 24)

    def update_progress(self, n):
        self.pb.setValue(self.pb.value()+n)

    def toggle_progress_bar(self):
        if self.config.show_progress_bar:
            self.pb.hide()
        else:
            self.pb.show()
        self.config.show_progress_bar = not self.config.show_progress_bar

    def setNNimageBgColor(self):
        """Allows the user to customise the background colour of non-native
        images."""
        self.config.nn_img_rgb = QtGui.QColorDialog.getColor(
                self.getNNimageBgColor(), self.centralwidget).getRgb()
        for button in self.pushButtonList.values():
             if not button.isNative:
                self.updateButtonColor(button)

    def toggleLoadLatestPage(self):
        self.config.load_latest_page = not self.config.load_latest_page

    def getNNimageBgColor(self):
            return apply(QtGui.QColor, self.config.nn_img_rgb)

    def showTranslations(self):
        if not self.searchKeywords or self.searchKeywords[0][1] == '':
            return
        if not self.aquery.translations:
            self.noTranslationsDialog()
            return
        self.translationsDialog = QtGui.QDialog()
        layout = QtGui.QVBoxLayout(self.translationsDialog)
        label = QtGui.QLabel("<br>Article <b> %s </b> appears also on the following %d Wikipedia sites:<br>" % (self.searchKeywords[0][1], len(self.aquery.translations)))
        layout.addWidget(label)
        self.translationsComboBox = QtGui.QComboBox(self.translationsDialog)
        self.translationsComboBox.setObjectName("translationsComboBox")
        for lang_short in self.aquery.translations:
            self.translationsComboBox.addItem(QtGui.QApplication.translate("dialog", str(wikipedia.languages[lang_short].encode("utf8")) , None, QtGui.QApplication.UnicodeUTF8))
        self.translationsComboBox.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum,
        QtGui.QSizePolicy.Maximum))

        self.translationsDialog.setWindowTitle("Translations")
        self.translationsDialog.setWindowIcon(QtGui.QIcon(':/logo.jpg'))
        self.okbutton = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        self.okbutton.setObjectName("okbutton")
        QtCore.QObject.connect(self.okbutton, QtCore.SIGNAL("accepted()"),
        self.chosenTranslation, QtCore.Qt.QueuedConnection)
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.translationsComboBox)
        hbox.addWidget(self.okbutton)
        layout.addLayout(hbox)
        label2 = QtGui.QLabel("<br>")
        layout.addWidget(label2)
        self.translationsDialog.setFixedSize(
                self.translationsDialog.sizeHint())
        self.translationsDialog.exec_()


    def chosenTranslation(self):
        """shows the article on a different wikipedia"""
        self.config.language = [item[0] for item in wikipedia.languages.items() \
                if item[1] == unicode(self.translationsComboBox.currentText())][0]
        if self.config.language not in languages.keys():
            languages[self.config.language] = unicode(self.translationsComboBox.currentText())
        keyword = self.aquery.translations[self.config.language]
        self.translationsDialog.accept()
        self.lineEdit.clear()
        self.lineEdit.insert(QtCore.QString(keyword))
        self.showCombobox()
        self.first_click()



    def noTranslationsDialog(self):
        """when the article does not appear on other Wikipedia sites"""
        self.translationsDialog = QtGui.QDialog()
        layout = QtGui.QVBoxLayout(self.translationsDialog)
        label = QtGui.QLabel("<br>Article <b> %s </b>  does not appear on other Wikipedia sites<br>" % self.searchKeywords[0][1])
        layout.addWidget(label)
        self.translationsDialog.setWindowTitle("Translations")
        self.translationsDialog.setWindowIcon(QtGui.QIcon(':/logo.jpg'))
        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        layout.addWidget(buttonBox)
        QtCore.QObject.connect(buttonBox, QtCore.SIGNAL(
                'accepted()'), self.translationsDialog,QtCore.SLOT("accept()"))
        self.translationsDialog.setFixedSize(
                self.translationsDialog.sizeHint())
        self.translationsDialog.exec_()



    def showCombobox(self):
        """creates the comboBox list with the different Wikipedia sites"""
        self.comboBox.clear()
        self.comboBox.addItem(QtGui.QApplication.translate(
                    "self.centralwidget", str(wikipedia.languages[self.config.language] \
                    .encode("utf8")) , None, \
                    QtGui.QApplication.UnicodeUTF8))
        for lang_short in languages:
            if lang_short != Ui_MainWindow.config.language:
                self.comboBox.addItem(QtGui.QApplication.translate(
                        "self.centralwidget", str(languages[lang_short] \
                        .encode("utf8")) , None, \
                        QtGui.QApplication.UnicodeUTF8))


    def showMyTextBrowser1Text(self):
        """sets the default text for MyTextBrowser1"""
        self.MyTextBrowser1.insertHtml("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
        "p, li { white-space: pre-wrap; }\n"
        "</style></head><body style=\" font-family:\'Helvetica\'; font-size:11pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">  CONTENTS <br> </span></p></body></html>")

    def showMyTextBrowser2Text(self):
        """sets the default text for MyTextBrowser2"""
        self.MyTextBrowser2.insertHtml("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
        "p, li { white-space: pre-wrap; }\n"
        "</style></head><body style=\" font-family:\'Helvetica\'; font-size:11pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">  LINKS <br> </span></p></body></html>")


def main():
    from config import Config
    net.URLLister.config = Config()
    reload(sys)
    sys.setdefaultencoding("utf8")
    app = QtGui.QApplication(sys.argv)
    window = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(window, app, net.URLLister.config)
    window.show()
    net.URLLister.config.save()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
