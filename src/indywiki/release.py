__version__ = '0.9.9'
__date__ = "April 2008"
__authors__ = "Markos Gogoulos <mgogoulos@gmail.com>, Serafeim Zanikolas " + \
    "<serzan@hellug.gr>"
__license__ = "General Public License (GPL) 2 or later"
