indywiki (0.9.9.1-7) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/watch: Use https protocol
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:12:20 -0500

indywiki (0.9.9.1-6) unstable; urgency=low

  * Migrate to dh_python2
    - Build-Depends: drop python-support, and add versioned python
    - remove debian/pyversions and add X-Python-Version in debian/control
    - debian/rules: invoke dh with python2 addon
  * debian/control:Maintainer: set to my debian email address
  * Bump Standards-Version to 3.9.2 (no changes)

 -- Serafeim Zanikolas <sez@debian.org>  Sun, 07 Aug 2011 20:10:19 +0200

indywiki (0.9.9.1-5) unstable; urgency=low

  * Bump Standards-Version to 3.8.4 (no changes)
  * Switch dpkg-source format to 3.0 (quilt)
    * debian/control: drop Depends: quilt
    * debian/rules: drop patch-related stuff
  * Revise debian/rules to use dh overrides and depend on debhelper >= 7.0.50~
  * Apply python 2.6 fixes (thanks to Jonathan Wiltshire for the patch)
      - do not install the indywiki script in /usr/local/ (Closes: #571476)
      - bump the minimum python-support dependency to 0.90
  * Add missing ${misc:Depends}
  * Add DEP-3 headers to patches

 -- Serafeim Zanikolas <serzan@hellug.gr>  Sun, 28 Feb 2010 13:02:34 +0100

indywiki (0.9.9.1-4) unstable; urgency=low

  * Rename "non-native" images to "nearby" images (Closes: #488464).
  * Switch patch management to quilt.
  * Add Uploaders and Vcs-* fields as it is now maintained as part of the
    python-apps team.

 -- Serafeim Zanikolas <serzan@hellug.gr>  Sun, 22 Feb 2009 02:28:54 +0000

indywiki (0.9.9.1-3) unstable; urgency=low

  * Add missing dependency python-pkg-resources (Closes: #488405)
  * Don't do anything when the user clicks cancel in the dialog box for
    changing the background colour of non-native images (Closes: #488411)

 -- Serafeim Zanikolas <serzan@hellug.gr>  Sun, 29 Jun 2008 00:04:54 +0100

indywiki (0.9.9.1-2) unstable; urgency=low

  * Correct dependency for PyQt4 (Closes: #487663)

 -- Serafeim Zanikolas <serzan@hellug.gr>  Mon, 23 Jun 2008 21:19:20 +0100

indywiki (0.9.9.1-1) unstable; urgency=low

  * Initial release (Closes: #473038)

 -- Serafeim Zanikolas <serzan@hellug.gr>  Tue, 10 Jun 2008 20:53:18 +0100


